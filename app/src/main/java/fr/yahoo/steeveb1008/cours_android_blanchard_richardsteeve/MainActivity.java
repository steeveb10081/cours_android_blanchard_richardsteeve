package fr.yahoo.steeveb1008.cours_android_blanchard_richardsteeve;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private CardView butt_tp1;
    private CardView butt_tp2;
    private CardView butt_tp3;
    private CardView butt_tp4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        butt_tp1=findViewById(R.id.id_butt_tp1);
        butt_tp2=findViewById(R.id.id_butt_tp2);
        butt_tp3=findViewById(R.id.id_butt_tp3);
        butt_tp4=findViewById(R.id.id_butt_tp4);

        butt_tp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("fr.yahoo.steeveb.mbds.haiti.tp01");
                startActivity(intent);
            }
        });

        butt_tp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("fr.yahoo.steeveb.mbds.haiti.tp02");
                startActivity(intent);
            }
        });


        butt_tp3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("fr.yahoo.steeveb.mbds.haiti.tp03");
                startActivity(intent);
            }
        });


        butt_tp4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("fr.yahoo.steeveb.mbds.haiti.tp04");
                startActivity(intent);
            }
        });



    }
}