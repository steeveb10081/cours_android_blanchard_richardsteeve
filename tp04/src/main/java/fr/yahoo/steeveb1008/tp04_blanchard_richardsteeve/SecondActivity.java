package fr.yahoo.steeveb1008.tp04_blanchard_richardsteeve;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {
    Button butt_fermer ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Log.d("CDV SecondActivity", "La méthode onCreate est appelée");

        butt_fermer=findViewById(R.id.butt_fermer);

        butt_fermer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("CDV SecondActivity", "La méthode onStart est appelée");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("CDV SecondActivity", "La méthode onResume est appelée");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("CDV SecondActivity", "La méthode onPause est appelée");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("CDV SecondActivity", "La méthode onStop est appelée");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("CDV SecondActivity", "La méthode onRestart est appelée");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("CDV SecondActivity", "La méthode onDestroy est appelée");
    }
}